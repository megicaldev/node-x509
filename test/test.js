var x509 = require('../index'),
  fs = require('fs'),
  path = require('path'),
  assert = require('assert');

// All cert files should read without throwing an error.
// Simple enough test, no?
fs.readdirSync(path.join(__dirname, 'certs')).forEach(function (file) {
  if (path.extname(file) != '.pem') {
    console.log("File: %s", file);
    console.log(x509.parseCert(path.join(__dirname, 'certs', file)));
    // x509.parseCert(path.join(__dirname, 'certs', file));
    console.log();
  }
});


//test chain with old crl
x509.verifyChain(
  fs.readFileSync(path.join(__dirname, './certs/vrk_user_cert.pem')),
  fs.readFileSync(path.join(__dirname, './certs/vrk_test_root.pem')),
  fs.readFileSync(path.join(__dirname, './certs/crl_old.pem')),
  fs.readFileSync(
    path.join(__dirname, './certs/vrk_test_intermediate.pem')
  ),
  fs.readFileSync(path.join(__dirname, './certs/crl_ca.pem')),
  function (err) {
    assert.throws(assert.ifError.bind(null, err), /CRL has expired/)
  }
);

//test Apple chain without crls
x509.verifyChain(
  fs.readFileSync(path.join(__dirname, './certs/CERT_IOS_TAMPERED.pem')),
  fs.readFileSync(path.join(__dirname, './certs/CERT_ROOTCA_IOS.pem')),
  '',
  fs.readFileSync(
    path.join(__dirname, './certs/CERT_CA_IOS.pem')
  ),
  '',
  function (err) {
    console.log('ERROR', err)
    // assert.throws(assert.ifError.bind(null, err), /CRL has expired/)
  }
);

//test revoked cert
x509.verify(
  fs.readFileSync(path.join(__dirname, './megical_certs/megical_user.crt')),
  fs.readFileSync(path.join(__dirname, './megical_certs/ca.crt')),
  fs.readFileSync(path.join(__dirname, './megical_certs/crl.pem')),
  function (err) {
    console.log('ERR', err)
    // assert.throws(assert.ifError.bind(null, err), /certificate revoked/)
  }
);

//test valid cert
x509.verify(
  fs.readFileSync(path.join(__dirname, './megical_certs/megical_user_cert_valid.pem')),
  fs.readFileSync(path.join(__dirname, './megical_certs/ca.crt')),
  fs.readFileSync(path.join(__dirname, './megical_certs/crl.pem')),
  function (err) {
    assert.throws(assert.ifError.bind(null, err), '')
  }

);
