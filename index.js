var x509 = require('./build/Release/x509');
var fs = require('fs');

exports.version = x509.version;
exports.getAltNames = x509.getAltNames;
exports.getSubject = x509.getSubject;
exports.getIssuer = x509.getIssuer;

exports.verifyChain = function (cert, caCert, crl, chainCert, chainCrl, cb) {
  if (!cert) {
    throw new TypeError('Certificate is required');
  }
  if (!caCert) {
    throw new TypeError('CA certificate is required');
  }
  /*
  if (!crl) {
    throw new TypeError('CRL is required');
  }
  */
  if (!chainCert) {
    throw new TypeError('chain certificate is required');
  }
  /*
  if (!chainCrl) {
    throw new TypeError('CRL of chain certificate is required');
  }
  */
  try {
    x509.verifyChain(cert, caCert, crl, chainCert, chainCrl);
    cb(null);
  } catch (verificationError) {
    cb(verificationError);
  }
};

exports.verify = function (cert, caCert, crl, cb) {
  if (!cert) {
    throw new TypeError('Certificate is required');
  }
  if (!caCert) {
    throw new TypeError('CA certificate is required');
  }
  if (!crl) {
    throw new TypeError('CRL is required');
  }

  try {
    x509.verify(cert, caCert, crl);
    cb(null);
  } catch (verificationError) {
    cb(verificationError);
  }
};


exports.parseCert = function (path) {
  var ret = x509.parseCert(path);
  var exts = {};
  for (var key in ret.extensions) {
    var newkey = key.replace('X509v3', '').replace(/ /g, '');
    newkey = newkey.slice(0, 1).toLowerCase() + newkey.slice(1);
    exts[newkey] = ret.extensions[key];
  }
  delete ret.extensions;
  ret.extensions = exts;
  return ret;
};
